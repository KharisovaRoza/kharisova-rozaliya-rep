package CalculatorProject;

public class CalcException extends Exception{

    private static final long serialVersionUID = 1L;
    private String errStr;  //  Описание ошибки

    public CalcException(String errStr) {
        super();
        this.errStr = errStr;
    }

    public String toString(){
        return this.errStr;
    }
}