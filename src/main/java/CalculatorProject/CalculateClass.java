package CalculatorProject;

public class CalculateClass {

    public static void main(String[] args) throws CalcException {
        CalculatorImpl c = new CalculatorImpl();


        try
        {
            System.out.println(c.evaluate("(-2+2)*2"));
            System.out.println(c.evaluate("(1+38)*4*4-5")); // Result: 151
            System.out.println(c.evaluate("7*6/2+8")); // Result: 29
            //System.out.println(c.evaluate("-12)1//(")); // Result: null
            System.out.println(c.evaluate("1+2+(1+(1+1))")); // Result: 6

        }
        catch(CalcException e)
        {
            System.out.println(e);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }


}


