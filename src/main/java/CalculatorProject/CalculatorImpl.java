package CalculatorProject;

public class CalculatorImpl {


    final int NONE = 0;         //  FAIL
    final int DELIMITER = 1;    //  Разделитель(+-*/^=, ")", "(" )
    final int VARIABLE = 2;     //  Переменная
    final int NUMBER = 3;       //  Число

    final int SYNTAXERROR = 0;  //  Синтаксическая ошибка (10 + 5 6 / 1)
    final int UNBALPARENS = 1;  //  Несовпадение количества открытых и закрытых скобок
    final int NOEXP = 2;        //  Отсутствует выражение при запуске анализатора
    final int DIVBYZERO = 3;    //  Ошибка деления на ноль

    final String EndOfFunction = "\0";

    private String exp;     //  Ссылка на строку с выражением
    private int IndexAtNow;     //  Текущий индекс в выражении
    private String token;   //  Сохранение текущей лексемы
    private int tokType;    //  Сохранение типа лексемы


    //  Получить следующую лексему
    private void getToken(){
        tokType = NONE;
        token = "";

        //  Проверка на окончание выражения
        if(IndexAtNow == exp.length()){
            token = EndOfFunction;
            return;
        }
        //  Проверка на пробелы, если есть пробел - игнорируем его.
        while(IndexAtNow < exp.length() && Character.isWhitespace(exp.charAt(IndexAtNow)))
            ++ IndexAtNow;
        //  Проверка на окончание выражения
        if(IndexAtNow == exp.length()){
            token = EndOfFunction;
            return;
        }
        if(isDelim(exp.charAt(IndexAtNow))){
            token += exp.charAt(IndexAtNow);
            IndexAtNow++;
            tokType = DELIMITER;
        }
        else if(Character.isLetter(exp.charAt(IndexAtNow))){
            while(!isDelim(exp.charAt(IndexAtNow))){
                token += exp.charAt(IndexAtNow);
                IndexAtNow++;
                if(IndexAtNow >= exp.length())
                    break;
            }
            tokType = VARIABLE;
        }
        else if (Character.isDigit(exp.charAt(IndexAtNow))){
            while(!isDelim(exp.charAt(IndexAtNow))){
                token += exp.charAt(IndexAtNow);
                IndexAtNow++;
                if(IndexAtNow >= exp.length())
                    break;
            }
            tokType = NUMBER;
        }
        else {
            token = EndOfFunction;
            return;
        }
    }

    private boolean isDelim(char charAt) {
        if((" +-/*%^=()".indexOf(charAt)) != -1)
            return true;
        return false;
    }

    //  Точка входа анализатора
    public double evaluate(String expstr) throws CalcException {

        double result;

        exp = expstr;
        IndexAtNow = 0;
        getToken();

        if(token.equals(EndOfFunction))
            handleErr(NOEXP);   //  Нет выражения

        //  Анализ и вычисление выражения
        result = evalExp2();

        if(!token.equals(EndOfFunction))
            handleErr(SYNTAXERROR);

        return result;
    }

    //  Сложить или вычислить два терма
    private double evalExp2() throws CalcException {

        char op;
        double result;
        double partialResult;
        result = evalExp3();
        while((op = token.charAt(0)) == '+' ||
                op == '-'){
            getToken();
            partialResult = evalExp3();
            switch(op){
                case '-':
                    result -= partialResult;
                    break;
                case '+':
                    result += partialResult;
                    break;
            }
        }
        return result;
    }

    //  Умножить или разделить два фактора
    private double evalExp3() throws CalcException {

        char op;
        double result;
        double partialResult;

        result = evalExp5();
        while((op = token.charAt(0)) == '*' ||
                op == '/' | op == '%'){
            getToken();
            partialResult = evalExp5();
            switch(op){
                case '*':
                    result *= partialResult;
                    break;
                case '/':
                    if(partialResult == 0.0)
                        handleErr(DIVBYZERO);
                    result /= partialResult;
                    break;
            }
        }
        return result;
    }

    //  Определить унарные + или -
    private double evalExp5() throws CalcException {
        double result;

        String op;
        op = " ";

        if((tokType == DELIMITER) && token.equals("+") ||
                token.equals("-")){
            op = token;
            getToken();
        }
        result = evalExp6();
        if(op.equals("-"))
            result =  -result;
        return result;
    }

    //  Обработать выражение в скобках
    private double evalExp6() throws CalcException {
        double result;

        if(token.equals("(")){
            getToken();
            result = evalExp2();
            if(!token.equals(")"))
                handleErr(UNBALPARENS);
            getToken();
        }
        else
            result = atom();
        return result;
    }

    //  Получить значение числа
    private double atom()   throws CalcException {

        double result = 0.0;
        switch(tokType){
            case NUMBER:
                try{
                    result = Double.parseDouble(token);
                }
                catch(NumberFormatException exc){
                    handleErr(SYNTAXERROR);
                }
                getToken();

                break;
            default:
                handleErr(SYNTAXERROR);
                break;
        }
        return result;
    }

    //  Кинуть ошибку
    private void handleErr(int nOEXP2) throws CalcException {

        String[] err  = {
                "Syntax error",
                "Unbalanced Parentheses",
                "No Expression Present",
                "Division by zero"
        };
        throw new CalcException(err[nOEXP2]);
    }


}

