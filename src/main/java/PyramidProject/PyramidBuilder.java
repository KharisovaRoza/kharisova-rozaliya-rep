package PyramidProject;

import java.util.*;

import static java.lang.Math.sqrt;

public class PyramidBuilder {
    public static void main(String[] args) throws PyramidException {
        Integer[] arr = {1, 4, 3, 2, 5, 6};

        List<Integer> array = Arrays.asList(arr);
        Collections.sort(array);
        checkPyramidMayBeBuild(array);
        int n = getNumberOfLevels(array.size());
        int[][] result = new int[n][2 * n - 1];
        Iterator<Integer> iterator = array.iterator();
        for (int i = 0; i < n; i++) {
            int j = 0;
            for (; j < n - i - 1; j++) {
                result[i][j] = 0;
            }
            for (; j < n + i; j++) {
                if (j == 0 || result[i][j - 1] == 0) {
                    result[i][j] = iterator.next();
                } else {
                    result[i][j] = 0;
                }
            }
            for (; j < result[i].length; j++) {
                result[i][j] = 0;
            }
        }
        for (int[] line : result) {
            for (int element : line) {
                System.out.print(element + "   ");
            }
            System.out.println();
        }
    }

    private static void checkPyramidMayBeBuild(List<Integer> array) throws PyramidException {
        if (array.contains(0)) {
            throw new PyramidException("В массиве есть нули!");
        }

        if (array.size() != new HashSet<Integer>(array).size()) {
            throw new PyramidException("Есть повторяющиеся элементы!");
        }
    }

    private static int getNumberOfLevels(int elementsNumber) throws PyramidException {
        double n = sqrt(0.25 + 2 * elementsNumber) - 0.5;
        if (n != Math.floor(n)) {
            throw new PyramidException("Неверное число элементов!");
        }
        return Math.toIntExact(Math.round(n));
    }


}
