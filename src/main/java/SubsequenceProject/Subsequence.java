package SubsequenceProject;

import java.util.Arrays;
import java.util.List;

public class Subsequence {
    public static void main(String[] args) {
        boolean b = find(Arrays.asList("A", "B", "C", "D"),
                Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
        System.out.println(b); // Result: true
    }

    private static boolean find(List<String> list1, List<String> list2) {
        int count = 0;
        int fall =0;

        for (int i = 0; i<list1.size();i++){
            String s = list1.get(i);

            for(int j=fall; j<list2.size(); j++){

                if (s == list2.get(j)) {
                    count++;
                    fall = j;
                    break;
                }

            }

        }

        if (count == list1.size()) return true;
        else return false;

    }
}
